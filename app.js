const app = require('./config/server');
let ip = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';


app.listen(8080, ip, (req, res) => {
    console.log("Listening on " + ip + ", port " + 8080);
});