const express = require('express');
const consing = require('consign');
const bodyParser = require('body-parser');

const app = express();
app.set('view engine', 'ejs');
app.set('views', './server/views');
app.use(express.static('./server/public'));

app.use(bodyParser.urlencoded({ extended: true }));


consing()
    .include('server/routes')
    .then('server/controllers')
    .into(app);

module.exports = app;