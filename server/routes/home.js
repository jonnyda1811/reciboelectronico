module.exports = (app) => {
    app.get('/', (req, res) => {
        app.server.controllers.home.index(app, req, res);
    });
    app.post('/login', (req, res) => {
        app.server.controllers.home.login(app, req, res);
    });
};